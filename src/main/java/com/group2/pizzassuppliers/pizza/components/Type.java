package com.group2.pizzassuppliers.pizza.components;

public enum Type {
    CHEESE, VEGGIE, MEAT, PEPPERONI
}

package com.group2.pizzassuppliers.pizza.components;

import java.math.BigDecimal;

public class Sauce extends Component {
    public Sauce(String name, BigDecimal price) {
        this.setName(name);
        this.setPrice(price);
    }

    @Override
    public String toString() {
        return "Sauce " + getName();
    }
}

package com.group2.pizzassuppliers.pizza.components;

import java.math.BigDecimal;

public abstract class Component {
    private String name;
    private BigDecimal price;

    Component() {
    }

    Component(String name, BigDecimal price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    void setPrice(BigDecimal price) {
        this.price = price;
    }
}
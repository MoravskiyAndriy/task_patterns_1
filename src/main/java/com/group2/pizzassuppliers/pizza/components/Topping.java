package com.group2.pizzassuppliers.pizza.components;

import java.math.BigDecimal;

public class Topping extends Component {
    public Topping(String name, BigDecimal price) {
        this.setName(name);
        this.setPrice(price);
    }

    @Override
    public String toString() {
        return getName();
    }
}

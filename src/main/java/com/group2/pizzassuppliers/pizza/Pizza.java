package com.group2.pizzassuppliers.pizza;

import com.group2.pizzassuppliers.pizza.components.Component;
import com.group2.pizzassuppliers.pizza.components.Type;

import java.math.BigDecimal;
import java.util.List;

public class Pizza {
    private String name;
    private Type type;
    private BigDecimal price;
    private List<Component> components;

    public Pizza(String name, String type, List<Component> components) {
        this.name = name;
        this.type = Type.valueOf(type.toUpperCase());
        this.components = components;
        BigDecimal finalprice = new BigDecimal(0);
        for (Component c : components) {
            finalprice = finalprice.add(c.getPrice());
        }
        price = finalprice;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public String getTypeString() {
        return type.toString();
    }

    public void setType(String type) {
        this.type = Type.valueOf(type.toUpperCase());
    }

    public BigDecimal getPrice() {
        return price;
    }
}

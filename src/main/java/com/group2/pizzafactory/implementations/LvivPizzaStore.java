package com.group2.pizzafactory.implementations;

import com.group2.PizzaCollection;
import com.group2.pizzafactory.PizzaStore;
import com.group2.pizzassuppliers.PizzaSupplier;
import com.group2.pizzassuppliers.implementations.julienne.juliennecity.LvivJulienneSupplier;
import com.group2.pizzassuppliers.implementations.meatplatter.meatplattercity.LvivMeatPlatterSupplier;
import com.group2.pizzassuppliers.implementations.peperoni.peperonicity.LvivPeperoniSupplier;
import com.group2.pizzassuppliers.implementations.proscuittocongunghi.proscuittoconfunghicity.LvivProsciuttoConFunghiSupplier;
import com.group2.pizzassuppliers.implementations.vegeteriano.vegeterianocity.LvivVegeterianoSupplier;

public class LvivPizzaStore extends PizzaStore {

    @Override
    protected PizzaSupplier createPizzaSupplier(PizzaCollection pizza, int size, double thickness) {
        PizzaSupplier supplier = null;

        if (pizza == PizzaCollection.PEPERONI) {
            supplier = new LvivPeperoniSupplier(size, thickness);
        } else if (pizza == PizzaCollection.JULIENNE) {
            supplier = new LvivJulienneSupplier(size, thickness);
        } else if (pizza == PizzaCollection.PROSCIUTTOCONFUNGHI) {
            supplier = new LvivProsciuttoConFunghiSupplier(size, thickness);
        } else if (pizza == PizzaCollection.MEATPLATTER) {
            supplier = new LvivMeatPlatterSupplier(size, thickness);
        } else if (pizza == PizzaCollection.VEGETERIANO) {
            supplier = new LvivVegeterianoSupplier(size, thickness);
        }

        return supplier;
    }
}

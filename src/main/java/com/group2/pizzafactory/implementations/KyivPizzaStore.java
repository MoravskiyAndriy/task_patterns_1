package com.group2.pizzafactory.implementations;

import com.group2.PizzaCollection;
import com.group2.pizzafactory.PizzaStore;
import com.group2.pizzassuppliers.PizzaSupplier;
import com.group2.pizzassuppliers.implementations.julienne.juliennecity.KyivJulienneSupplier;
import com.group2.pizzassuppliers.implementations.meatplatter.meatplattercity.KyivMeatPlatterSupplier;
import com.group2.pizzassuppliers.implementations.peperoni.peperonicity.KyivPeperoniSupplier;
import com.group2.pizzassuppliers.implementations.proscuittocongunghi.proscuittoconfunghicity.KyivProsciuttoConFunghiSupplier;
import com.group2.pizzassuppliers.implementations.vegeteriano.vegeterianocity.KyivVegeterianoSupplier;

public class KyivPizzaStore extends PizzaStore {

    @Override
    protected PizzaSupplier createPizzaSupplier(PizzaCollection pizza, int size, double thickness) {
        PizzaSupplier supplier = null;

        if (pizza == PizzaCollection.PEPERONI) {
            supplier = new KyivPeperoniSupplier(size, thickness);
        } else if (pizza == PizzaCollection.JULIENNE) {
            supplier = new KyivJulienneSupplier(size, thickness);
        } else if (pizza == PizzaCollection.PROSCIUTTOCONFUNGHI) {
            supplier = new KyivProsciuttoConFunghiSupplier(size, thickness);
        } else if (pizza == PizzaCollection.MEATPLATTER) {
            supplier = new KyivMeatPlatterSupplier(size, thickness);
        } else if (pizza == PizzaCollection.VEGETERIANO) {
            supplier = new KyivVegeterianoSupplier(size, thickness);
        }

        return supplier;
    }
}

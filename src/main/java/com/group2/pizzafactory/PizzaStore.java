package com.group2.pizzafactory;

import com.group2.PizzaCollection;
import com.group2.pizzassuppliers.PizzaSupplier;
import com.group2.pizzassuppliers.pizza.Pizza;

import java.math.BigDecimal;

public abstract class PizzaStore {
    protected abstract PizzaSupplier createPizzaSupplier(PizzaCollection city, int size, double thickness);

    public BigDecimal getPrice(PizzaCollection city, int size, double thickness) {
        PizzaSupplier supplier = createPizzaSupplier(city, size, thickness);
        return supplier.getPrice();
    }

    public Pizza process(PizzaCollection city, int size, double thickness) {
        PizzaSupplier supplier = createPizzaSupplier(city, size, thickness);
        supplier.prepare();
        supplier.bake();
        supplier.cut();
        supplier.box();
        supplier.send();
        return supplier.givePizza();
    }
}
